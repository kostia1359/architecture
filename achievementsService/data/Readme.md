#Directories

Here will these directories and files:

* models
    * achievement
    * userAchievement
* repositories
    * achievementRepository
    * userAchievementRepository
    * baseRepository
* migrations
    * data-migrations
    * etc
* seed-data
    * achievement
    * userAchievement
* seeders
    * achievement.seeder
    * userAchievement.seeder

Depending on chosen technologies and our needs files may vary. Also, we can add directories like **helpers** when required
