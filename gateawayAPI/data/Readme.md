#Directories

Here will these directories and files:

* models
    * user
* repositories
    * userRepository
    * baseRepository
* migrations
    * data-migrations
    * etc
* seed-data
    * user
* seeders
    * user.seeder

Depending on chosen technologies and our needs files may vary. Also, we can add directories like **helpers** when required
