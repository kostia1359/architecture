#Directories

Here will these directories and files:

* models
    * post
* repositories
    * postRepository
    * baseRepository
* migrations
    * data-migrations
    * etc
* seed-data
    * post
* seeders
    * post.seeder

Depending on chosen technologies and our needs files may vary. Also, we can add directories like **helpers** when required
