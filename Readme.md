# Architecture
I have chosen microservices architecture because our client is very ambitious and he wants to invest in this application a lot of money, so we can afford to create microservices, except for cheaper monolith.

We do not have a lot of time for creating MVC, so we need a big team. Microservices support different programming languages and technologies, so we can create a good team much faster. 

Our system needs stable work,if even one of the services is broken(for example user still can do all stuff when achievements do not work correctly) and this can be provided only by microservices. 

We consider, that our client may wish to add a new feature(like provide room in a hotel for VIP clients) and we need to update the project very often, because we don't know market needs(this is the first project in this domain)
# Communication protocol
As we do not have a lot of time, and we have a big team, I have chosen the HTTP protocol and REST API. This protocol has a big community. There are a lot of libraries for working with HTTP protocol. Also, our services do not need anything greater, than HTTP, but when we can simply add a new protocol when required

