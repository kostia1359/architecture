#Directories

Here will these directories and files:

* models
    * payment
* repositories
    * paymentRepository
    * baseRepository
* migrations
    * data-migrations
    * etc
* seed-data
    * payment
* seeders
    * payment.seeder

Depending on chosen technologies and our needs files may vary. Also, we can add directories like **helpers** when required
