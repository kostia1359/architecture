# Directories

Here will these directories and files:

* routes
    * authRoute
* middlewares
    * validationMiddleware
    * roleMiddleware
    * errorHandlerMiddleware
* services
    * userService
* http
    * test.http

Depending on chosen technologies and our needs files may vary. Also, we can add directories like **helpers** when required
