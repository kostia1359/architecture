# Directories

Here will these directories and files:

* routes
    * menuRoute
    * orderRoute
* middlewares
    * validationMiddleware
    * errorHandlerMiddleware
* services
    * orderService
    * menuService
* http
    * test.http

Depending on chosen technologies and our needs files may vary. Also, we can add directories like **helpers** when required
