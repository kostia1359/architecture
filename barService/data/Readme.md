#Directories

Here will these directories and files:

* models
    * menu
* repositories
    * menuRepository
    * baseRepository
* migrations
    * data-migrations
    * etc
* seed-data
    * menu
* seeders
    * menu.seeder

Depending on chosen technologies and our needs files may vary. Also, we can add directories like **helpers** when required
