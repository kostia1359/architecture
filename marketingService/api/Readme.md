# Directories

Here will these directories and files:

* routes
    * marketingRoute
* middlewares
    * errorHandlerMiddleware
* services
    * emailService
* http
    * test.http

Depending on chosen technologies and our needs files may vary. Also, we can add directories like **helpers** when required
