#Directories

Here will these directories and files:

* models
    * abonnement
* repositories
    * abonnementRepository
    * baseRepository
* migrations
    * data-migrations
    * etc
* seed-data
    * abonnement
* seeders
    * abonnement.seeder

Depending on chosen technologies and our needs files may vary. Also, we can add directories like **helpers** when required
