#Directories

Here will these directories and files:

* models
    * equipment
* repositories
    * equipmentRepository
    * baseRepository
* migrations
    * data-migrations
    * etc
* seed-data
    * equipment
* seeders
    * equipment.seeder

Depending on chosen technologies and our needs files may vary. Also, we can add directories like **helpers** when required
